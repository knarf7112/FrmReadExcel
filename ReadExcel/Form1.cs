﻿using System;
using System.Data;
using System.Windows.Forms;
//---------------------------------
//using Microsoft.Office.Interop.Excel;//add excel reference
using System.Data.OleDb;
namespace ReadExcel
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            this.openFileDialog1.InitialDirectory = "d:\\";
            this.openFileDialog1.Filter = "Excel file (*.xls)|*.xls|Excel(*.xlsx)|*.xlsx";
            this.dataGridView1.DataSource = null;
            if (this.openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (this.openFileDialog1.OpenFile() != null)
                {
                    this.txtPath.Text = this.openFileDialog1.FileName;
                    try
                    {
                        //Microsoft.Office.Interop.Excel.Application xlapp = new Microsoft.Office.Interop.Excel.Application();
                        //xlapp.Visible = true;
                        //Workbook xlwork = xlapp.Workbooks.Open(this.openFileDialog1.FileName);
                        //Worksheet xlsheet = xlwork.Worksheets.get_Item("例外記錄檔20141120E005餘額比對異常");
                        //Range rg = xlsheet.get_Range("A1", "A10");

                        DataSet ds = Parse(this.openFileDialog1.FileName);
                        this.dataGridView1.DataSource = ds.Tables[0];

                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
        }

        static DataSet Parse(string fileName)
        {
            //string connectionString = string.Format("provider=Microsoft.Jet.OLEDB.4.0; data source={0};Extended Properties='Excel 8.0;HDR=Yes;IMEX=1'", fileName);// .xlsx檔完全抓不到....
            string connectionString = string.Format("provider=Microsoft.ACE.OLEDB.12.0; data source={0};Extended Properties='Excel 8.0;HDR=Yes;IMEX=1'", fileName);//'Excel 8.0;HDR=Yes;IMEX=1'一定要加''沒加就->格式錯誤
            DataSet data = new DataSet();

            foreach (var sheetName in GetExcelSheetNames(connectionString))
            {
                using (OleDbConnection con = new OleDbConnection(connectionString))
                {
                    var dataTable = new DataTable();
                    string query = string.Format("SELECT * FROM [{0}]", sheetName);
                    con.Open();
                    OleDbDataAdapter adapter = new OleDbDataAdapter(query, con);
                    adapter.Fill(dataTable);
                    data.Tables.Add(dataTable);
                }
            }

            return data;
        }

        static string[] GetExcelSheetNames(string connectionString)
        {
            try
            {
                OleDbConnection con = null;
                DataTable dt = null;
                con = new OleDbConnection(connectionString);
                con.Open();
                dt = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

                if (dt == null)
                {
                    return null;
                }

                String[] excelSheetNames = new String[dt.Rows.Count];
                int i = 0;

                foreach (DataRow row in dt.Rows)
                {
                    excelSheetNames[i] = row["TABLE_NAME"].ToString();
                    i++;
                }

                return excelSheetNames;
            }
            catch (Exception ex) { }
            return null;
        }
    }
}
